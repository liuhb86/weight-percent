//
//  ViewController.swift
//  WeightPercent
//
//  Created by Liu, Hongbo on 10/14/17.
//  Copyright © 2017 Liu, Hongbo. All rights reserved.
//

import UIKit
import HealthKit

var baseline = 0.0

class ViewController: UIViewController {
    
    
    @IBOutlet weak var percetage: UILabel!
    
    let healthStore = HKHealthStore()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        authorizeHealthKit()

        let fileName = "baseline"
        let DocumentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        let fileURL = DocumentDirURL.appendingPathComponent(fileName).appendingPathExtension("txt")

        var readString = "" // Used to store the file contents
        do {
            // Read the file contents
            readString = try String(contentsOf: fileURL)
            baseline = Double(readString)!
        } catch let error as NSError {
            print("Failed reading from URL: \(fileURL), Error: " + error.localizedDescription)
        }
        print("File Text: \(readString)")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("view WillAppear triggered")
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(applicationDidBecomeActive),
                                               name: .UIApplicationDidBecomeActive,
                                               object: nil)
        getRecentWeight()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self,
                                                  name: .UIApplicationDidBecomeActive,
                                                  object: nil)
    }

    func applicationDidBecomeActive() {
        print("applicationDidBecomeActive triggered")
        getRecentWeight()
    }
    
    func calcPercentage(weight : Double) {
        let loss = (baseline - weight) / baseline * 100
        percetage.text = String(format:"%.4f%%", loss)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func authorizeHealthKit() {
        let healthKitTypesToRead: Set<HKObjectType> = [HKObjectType.quantityType(forIdentifier: .bodyMass)!]
        HKHealthStore().requestAuthorization(toShare: nil, read: healthKitTypesToRead) { (success, error) in
            print(success)
        }
    }
    
    func getRecentWeight() {
        //1. Use HKQuery to load the most recent samples.
        let mostRecentPredicate = HKQuery.predicateForSamples(withStart: Date.distantPast,
                                                              end: Date(),
                                                              options: .strictEndDate)
        
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate,
                                              ascending: false)
        
        let limit = 1
        
        let sampleQuery = HKSampleQuery(sampleType: HKSampleType.quantityType(forIdentifier: .bodyMass)!,
                                        predicate: mostRecentPredicate,
                                        limit: limit,
                                        sortDescriptors: [sortDescriptor]) { (query, samples, error) in
                                            
            //2. Always dispatch to the main thread when complete.
            DispatchQueue.main.async {
                guard let samples = samples,
                let mostRecentSample = samples.first as? HKQuantitySample else {
                    print("no weight record")
                    return
                }
                
                let weightInKilograms = mostRecentSample.quantity.doubleValue(for: HKUnit.gramUnit(with: .kilo))
                print("last weight:", weightInKilograms)
                self.calcPercentage(weight: weightInKilograms)
            }
        }
        HKHealthStore().execute(sampleQuery)
    }

}

