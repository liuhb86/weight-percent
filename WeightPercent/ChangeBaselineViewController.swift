//
//  ViewController.swift
//  WeightPercent
//
//  Created by Liu, Hongbo on 10/14/17.
//  Copyright © 2017 Liu, Hongbo. All rights reserved.
//

import UIKit

class ChangeBaselineViewController: UIViewController {
    
    @IBOutlet weak var newBaseline: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func change(_ sender: Any) {
        if newBaseline.text == nil {
            return
        }
        let newValue = Double(newBaseline.text!)
        if newValue == nil {
            return
        }
        baseline = newValue!
        
        let fileName = "baseline"
        let DocumentDirURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        let fileURL = DocumentDirURL.appendingPathComponent(fileName).appendingPathExtension("txt")
        print("FilePath: \(fileURL.path)")
        
        let writeString = String(newValue!)
        do {
            // Write to the file
            try writeString.write(to: fileURL, atomically: true, encoding: String.Encoding.utf8)
        } catch let error as NSError {
            print("Failed writing to URL: \(fileURL), Error: " + error.localizedDescription)
        }
        
        self.navigationController!.popViewController(animated: true)

    }
    
}

